package gen;

import java.util.LinkedList;

import graph.Edge;
import graph.Graph;
import grid.Grid;

public class GraphGenerator {
	
	public static Graph<Edge> gridGraph(int nbrow, int nbcol) {
		Graph<Edge> g = new Graph<>(nbrow * nbcol);
		for(int i = 0; i < g.V(); i++) {
			LinkedList<Integer> adj = Grid.adjacentIndexes(nbrow, nbcol, i);
			for(int j : adj) {
				Edge e = new Edge(i, j);
				Edge er = new Edge(j, i);
				g.addEdge(e);
				g.addEdge(er);
			}
		}
		return g;
	}
	

}
