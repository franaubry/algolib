package unitTests;


import org.junit.Test;
import dataStructures.SequenceSet;

import static org.junit.Assert.*;

import java.util.Random;

public class SequenceSetTest {
	
	@Test
	public void testRandomElements() {
		Random rnd = new Random();
		for(int maxval = 1; maxval <= 10; maxval++) {
			SequenceSet S = new SequenceSet(maxval);
			for(int i = 0; i < 1000; i++) {
				int len = rnd.nextInt(10 + 1);
				int[] a = new int[len];
				for(int j = 0; j < len; j++) {
					a[j] = rnd.nextInt(maxval);
				}
				S.add(a);
				assertTrue(S.contains(a));
			}
		}
	}

}
