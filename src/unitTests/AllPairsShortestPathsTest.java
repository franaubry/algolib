package unitTests;

import org.junit.Test;
import static org.junit.Assert.*;


import graph.AllPairsShortestPaths;
import graph.Graph;
import graph.WeightedEdge;


import java.util.BitSet;


public class AllPairsShortestPathsTest {
	
	@Test
	public void test1() {
		Graph<WeightedEdge> g = new Graph<>(4);
		g.addEdge(new WeightedEdge(0, 1, 1));
		g.addEdge(new WeightedEdge(0, 2, 1));
		g.addEdge(new WeightedEdge(1, 3, 1));
		g.addEdge(new WeightedEdge(2, 3, 1));
		AllPairsShortestPaths<WeightedEdge> apsp = new AllPairsShortestPaths<>(g);
		BitSet[] dag = new BitSet[g.V()];
		for(int x = 0; x < g.V(); x++) {
			dag[x] = apsp.getDagEdges(0, x);
		}
		assertTrue(dag[0].toString().equals("{}"));
		assertTrue(dag[1].toString().equals("{0}"));
		assertTrue(dag[2].toString().equals("{1}"));
		assertTrue(dag[3].toString().equals("{0, 1, 2, 3}"));	
	}
	
	@Test
	public void test2() {
		Graph<WeightedEdge> g = new Graph<>(5);
		g.addEdge(new WeightedEdge(0, 1, 1));
		g.addEdge(new WeightedEdge(0, 2, 1));
		g.addEdge(new WeightedEdge(1, 3, 1));
		g.addEdge(new WeightedEdge(2, 3, 1));
		g.addEdge(new WeightedEdge(4, 1, 1));
		AllPairsShortestPaths<WeightedEdge> apsp = new AllPairsShortestPaths<>(g);
		BitSet[] dag = new BitSet[g.V()];
		for(int x = 0; x < g.V(); x++) {
			dag[x] = apsp.getDagEdges(0, x);
		}
		assertTrue(dag[0].toString().equals("{}"));
		assertTrue(dag[1].toString().equals("{0}"));
		assertTrue(dag[2].toString().equals("{1}"));
		assertTrue(dag[3].toString().equals("{0, 1, 2, 3}"));	
		assertTrue(dag[4].toString().equals("{}"));
		
		dag = new BitSet[g.V()];
		for(int x = 0; x < g.V(); x++) {
			dag[x] = apsp.getDagEdges(4, x);
		}
		assertTrue(dag[0].toString().equals("{}"));
		assertTrue(dag[1].toString().equals("{4}"));
		assertTrue(dag[2].toString().equals("{}"));
		assertTrue(dag[3].toString().equals("{2, 4}"));	
		assertTrue(dag[4].toString().equals("{}"));	
	}
	
	@Test
	public void test3() {
		Graph<WeightedEdge> g = new Graph<>(4);
		g.addEdge(new WeightedEdge(0, 1, 1));
		g.addEdge(new WeightedEdge(0, 2, 1));
		g.addEdge(new WeightedEdge(1, 3, 1));
		g.addEdge(new WeightedEdge(2, 3, 1));
		AllPairsShortestPaths<WeightedEdge> apsp = new AllPairsShortestPaths<>(g);
		@SuppressWarnings("unchecked")
		Graph<WeightedEdge>[] dag = new Graph[g.V()];
		for(int x = 0; x < g.V(); x++) {
			dag[x] = apsp.getDag(0, x);
		}
		assertTrue(dag[0].edgeSet().toString().equals("{}"));
		assertTrue(dag[1].edgeSet().toString().equals("{0}"));
		assertTrue(dag[2].edgeSet().toString().equals("{1}"));
		assertTrue(dag[3].edgeSet().toString().equals("{0, 1, 2, 3}"));	
	}

	@SuppressWarnings("unchecked")
	@Test
	public void test4() {
		Graph<WeightedEdge> g = new Graph<>(5);
		g.addEdge(new WeightedEdge(0, 1, 1));
		g.addEdge(new WeightedEdge(0, 2, 1));
		g.addEdge(new WeightedEdge(1, 3, 1));
		g.addEdge(new WeightedEdge(2, 3, 1));
		g.addEdge(new WeightedEdge(4, 1, 1));
		AllPairsShortestPaths<WeightedEdge> apsp = new AllPairsShortestPaths<>(g);
		Graph<WeightedEdge>[] dag = new Graph[g.V()];
		for(int x = 0; x < g.V(); x++) {
			dag[x] = apsp.getDag(0, x);
		}
		assertTrue(dag[0].edgeSet().toString().equals("{}"));
		assertTrue(dag[1].edgeSet().toString().equals("{0}"));
		assertTrue(dag[2].edgeSet().toString().equals("{1}"));
		assertTrue(dag[3].edgeSet().toString().equals("{0, 1, 2, 3}"));	
		assertTrue(dag[4].edgeSet().toString().equals("{}"));
		dag = new Graph[g.V()];
		for(int x = 0; x < g.V(); x++) {
			dag[x] = apsp.getDag(4, x);
		}
		assertTrue(dag[0].edgeSet().toString().equals("{}"));
		assertTrue(dag[1].edgeSet().toString().equals("{4}"));
		assertTrue(dag[2].edgeSet().toString().equals("{}"));
		assertTrue(dag[3].edgeSet().toString().equals("{2, 4}"));	
		assertTrue(dag[4].edgeSet().toString().equals("{}"));	
	}
	
}
