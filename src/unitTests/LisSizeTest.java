package unitTests;

import org.junit.Test;

import classic.LIS;

import static org.junit.Assert.*;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.Scanner;


public class LisSizeTest {
	
	@Test
	public void test1() {
		String path = "/home/yunoac/Dropbox/ContestClass/LIS/data/secret/";
		File dir = new File(path);
		for(String f : dir.list()) {
			System.out.println(f);
			if(!f.endsWith(".in")) continue;
			try {
				Scanner reader = new Scanner(new FileReader(path + f));
				ArrayList<Integer> a = new ArrayList<>();
				while(reader.hasNext()) {
					a.add(reader.nextInt());
				}
				System.out.println(a);
				reader.close();
				reader = new Scanner(new FileReader(path + f.replace(".in", ".ans")));
				int expected = reader.nextInt();
				System.out.println(expected);
				reader.close();
				ArrayList<Integer> lis = LIS.lds(a, true);
				System.out.println(lis);
				assertTrue(expected == lis.size());
			} catch (FileNotFoundException e) {
				e.printStackTrace();
			}
			
		}
	}

}
