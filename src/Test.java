import java.util.Arrays;

import graph.FloydWarshall;
import graph.Hungarian;

public class Test {
	
	public static void main(String[] args) {
		
		double[][] A = new double[3][3];
		A[0] = new double[] {7, 7, 4};
		A[1] = new double[] {2, 2, 5};
		A[2] = new double[] {4, 5, 3};
		
		Hungarian H = new Hungarian(A, true);
		System.out.println(Arrays.toString(H.getAssignment()));
		double ans = H.getAssignmentValue();
		System.out.println(ans);
	}

}
