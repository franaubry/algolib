package grid;

import java.util.LinkedList;

import utils.Constants;

public class Grid {
	
	/**
	 * Compute the index of position (r, c) in a grid
	 * with nbcol columns. The top left corner is position 
	 * (0, 0) with index 0.
	 * 
	 * Example:
	 *  
	 * A 2 by 5 grid is numbered follows
	 * 
	 *    0 1 2 3 4
	 *    ---------
	 * 0 |0 1 2 3 4
	 * 1 |5 6 7 8 9
	 *
	 * getIndex(2, 5, 1, 3) = 8
	 * 
	 * @param nbcol the number of columns in the grid
	 * @param r the row of the grid
	 * @param c the column of the grid
	 * 
	 * @return the index of position (r, c) in a grid wtih nbrow rows
	 * and nbcol columns
	 */
	public static int getIndex(int nbcol, int r, int c) {
		return r * nbcol + c;
	}
	
	/**
	 * @param nbcol the number of columns in the grid
	 * @param index the index for which we want to find the row
	 * @return the row of the position at index
	 */
	public static int getRow(int nbcol, int index) {
		return index / nbcol;
	}
	
	/**
	 * @param nbcol the number of columns in the grid
	 * @param index the index for which we want to find the row
	 * @return the column of the position at index
	 */
	public static int getCol(int nbcol, int index) {
		return index % nbcol;
	}
	
	/**
	 * 
	 * @param nbrow
	 * @param nbcol
	 * @param r
	 * @param c
	 * @return true if position (r, c) is within the bound of a nbrow
	 * by nbcol grid.
	 */
	public static boolean inBounds(int nbrow, int nbcol, int r, int c) {
		return 0 <= r && r < nbrow && 0 <= c && c < nbcol;
	}
	
	/**
	 *
	 * @param nbrow
	 * @param nbcol
	 * @param index
	 * @return return the indexes of the positions adjacent to index
	 */
	public static LinkedList<Integer> adjacentIndexes(int nbrow, int nbcol, int index) {
		int row = getRow(nbcol, index);
		int col = getCol(nbcol, index);
		LinkedList<Integer> L = new LinkedList<>();
		for(int[] d : Constants.dir) {
			int r = row + d[0];
			int c = col + d[1];
			if(inBounds(nbrow, nbcol, r, c)) {
				L.addFirst(getIndex(nbcol, r, c));
			}
		}
		return L;
	}
	
	
	
}
