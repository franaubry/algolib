package dataStructures;

import java.util.Comparator;

/**
 * Class that represents an ordered pair of integers.
 * 
 * @author f.aubry@uclouvain.be
 */
public class IntTriple {
	
	public int x, y, z;
	
	public IntTriple(int x, int y, int z) {
		this.x = x;
		this.y = y;
		this.z = z;
	}
	
	public boolean equals(Object other) {
		if(other instanceof IntTriple) {
			IntTriple o = (IntTriple)other;
			return x == o.x && y == o.y && z == o.z;
		}
		return false;
	}
	
	public int hashCode() {
		return x + 31 * y + 31 * 31 * z;
	}
	
	public String toString() {
		return String.format("(%d, %d, %d)", x, y, z);
	}
	
	public static class Xcmp implements Comparator<IntTriple> {

		public int compare(IntTriple a, IntTriple b) {
			return a.x - b.x;
		}

	}

	public static class XcmpRev implements Comparator<IntTriple> {

		public int compare(IntTriple a, IntTriple b) {
			return -(a.x - b.x);
		}

	}
	
	
	public static class Ycmp implements Comparator<IntTriple> {

		public int compare(IntTriple a, IntTriple b) {
			return a.y - b.y;
		}

	}

	public static class YcmpRev implements Comparator<IntTriple> {

		public int compare(IntTriple a, IntTriple b) {
			return -(a.y - b.y);
		}

	}
	
	public static class Zcmp implements Comparator<IntTriple> {

		public int compare(IntTriple a, IntTriple b) {
			return a.z - b.z;
		}

	}

	public static class ZcmpRev implements Comparator<IntTriple> {

		public int compare(IntTriple a, IntTriple b) {
			return -(a.z - b.z);
		}

	}

}
 