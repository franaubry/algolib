package dataStructures;

import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;

/**
 * Data structure to aggregate objects by key. Maintains a map K -> list of L so
 * that each key can correspond to several values.
 * 
 * @author f.aubry@uclouvain.be
 */
public class BucketSet<K, V> {

	private HashMap<K, HashSet<V>> M;

	/*
	 * Create an empty bucket map.
	 */
	public BucketSet() {
		M = new HashMap<>();
	}
	
	/*
	 * Add value v to the list corresponding to key k.
	 */
	public void put(K k, V v) {
		HashSet<V> S = M.get(k);
		if(S == null) {
			S = new HashSet<>();
			M.put(k, S);
		}
		S.add(v);
	}
	
	/*
	 * Get the number of values associated with key k.
	 */
	public int size(K k) {
		HashSet<V> S = M.get(k);
		if(S == null) return 0;
		return S.size();
	}
	
	/*
	 * Check whether the map contains key k.
	 */
	public boolean contains(K k) {
		return M.containsKey(k);
	}
	
	/*
	 * Check whether the bucket k has value v.
	 */
	public boolean contains(K k, V v) {
		return M.containsKey(k) && M.get(k).contains(v);
	}
	
	/*
	 * Get the list of values associated with key k.
	 */
	public HashSet<V> get(K k) {
		return M.get(k);
	}
	
	/*
	 * Get the set of all keys.
	 */
	public Set<K> keySet() {
		return M.keySet();
	}
	
	/*
	 * Remove value v from bucket k.
	 */
	public void remove(K k, V v) {
		M.get(k).remove(v);
	}
	
	public Collection<HashSet<V>> values() {
		return M.values();
	}
	
	/*
	 * Create a string representation of the bucket map.
	 */
	public String toString() {
		StringBuilder sb = new StringBuilder();
		for(K k : keySet()) {
			sb.append(k + ": " + M.get(k) + "\n");
		}
		return sb.toString();
	}
	
}
