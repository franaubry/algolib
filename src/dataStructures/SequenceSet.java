package dataStructures;

import java.util.Arrays;
import java.util.LinkedList;

public class SequenceSet {
	
	public static void main(String[] args) {
		SequenceSet S = new SequenceSet(4);
		S.add(new int[] {0, 1, 2, 3});
		S.add(new int[] {1, 2, 3});
		
		System.out.println(S.contains(new int[] {0, 1, 2, 3}));
		System.out.println(S.contains(new int[] {1, 2, 3}));
		System.out.println(S.contains(new int[] {2, 3}));
		System.out.println(S.contains(new int[] {3}));
		System.out.println(S.contains(new int[] {0, 1, 2}));
		System.out.println(S.contains(new int[] {0, 1}));
		System.out.println(S.contains(new int[] {0}));
		
		S.add(new int[] {0, 1, 2});
		
		System.out.println(S.contains(new int[] {0, 1, 2, 3}));
		System.out.println(S.contains(new int[] {1, 2, 3}));
		System.out.println(S.contains(new int[] {2, 3}));
		System.out.println(S.contains(new int[] {3}));
		System.out.println(S.contains(new int[] {0, 1, 2}));
		System.out.println(S.contains(new int[] {0, 1}));
		System.out.println(S.contains(new int[] {0}));
		
		
		System.out.println(S);
	}
	
	private int maxval;
	private Node root;
	
	public SequenceSet(int maxval) {
		this.maxval = maxval;
		root = new Node(maxval);
	}
	
	public void add(int[] a) {
		Node cur = root;
		for(int i = 0; i < a.length; i++) {
			if(cur.children[a[i]] == null) {
				cur.children[a[i]] = new Node(maxval);
			}
			cur = cur.children[a[i]];
		}
		cur.terminal = true;
	}
	
	public boolean contains(int[] a) {
		Node cur = root;
		for(int i = 0; i < a.length; i++) {
			if(cur.children[a[i]] == null) return false;
			cur = cur.children[a[i]];
		}
		return cur != null && cur.terminal;
	}
	
	private void buildString(Node cur, StringBuilder sb, LinkedList<Integer> a) {
		if(cur.terminal) {
			sb.append(a + "\n");
		}
		for(int i = 0; i < cur.children.length; i++) {
			if(cur.children[i] != null) {
				a.addLast(i);
				buildString(cur.children[i], sb, a);
				a.removeLast();
			}
		}
	}
	
	public String toString() {
		StringBuilder sb = new StringBuilder();
		LinkedList<Integer> a = new LinkedList<>();
		buildString(root, sb, a);
		return sb.toString();
	}
	
	private class Node {
		
		private Node[] children;
		private boolean terminal;
		
		public Node(int maxval) {
			children = new Node[maxval];
			terminal = false;
		}
		
		public String toString() {
			return Arrays.toString(children);
		}
	}

}
