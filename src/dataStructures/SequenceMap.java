package dataStructures;

import java.util.Arrays;
import java.util.LinkedList;

public class SequenceMap<E> {
	
	private int[] nodelen;
	private Node<E> root;
	
	public SequenceMap(int maxval, int maxlen) {
		this.nodelen = new int[maxlen + 1];
		Arrays.fill(this.nodelen, maxval + 1);
		this.nodelen[maxlen] = 0;
		root = new Node<>(this.nodelen[0]);
	}
	
	public SequenceMap(int[] maxval) {
		this.nodelen = new int[maxval.length + 1];
		for(int i = 0; i < maxval.length; i++) {
			this.nodelen[i] = maxval[i] + 1;
		}
		this.nodelen[maxval.length] = 0;
		root = new Node<>(this.nodelen[0]);
	}
	
	public void add(int[] a, E value) {
		Node<E> cur = root;
		for(int i = 0; i < a.length; i++) {
			if(cur.children[a[i]] == null) {
				cur.children[a[i]] = new Node<>(nodelen[i + 1]);
			}
			cur = cur.children[a[i]];
		}
		cur.terminal = true;
		cur.value = value;
	}
	
	public boolean contains(int[] a) {
		Node<E> cur = root;
		for(int i = 0; i < a.length; i++) {
			if(cur.children[a[i]] == null) return false;
			cur = cur.children[a[i]];
		}
		return cur != null && cur.terminal;
	}
	
	public E get(int[] a) {
		Node<E> cur = root;
		for(int i = 0; i < a.length; i++) {
			if(cur.children[a[i]] == null) return null;
			cur = cur.children[a[i]];
		}
		if(cur != null && cur.terminal) return cur.value;
		return null;
	}
	
	private void buildString(Node<E> cur, StringBuilder sb, LinkedList<Integer> a) {
		if(cur.terminal) {
			sb.append(a + ": " + cur.value +  "\n");
		}
		for(int i = 0; i < cur.children.length; i++) {
			if(cur.children[i] != null) {
				a.addLast(i);
				buildString(cur.children[i], sb, a);
				a.removeLast();
			}
		}
	}
	
	public String toString() {
		StringBuilder sb = new StringBuilder();
		LinkedList<Integer> a = new LinkedList<>();
		buildString(root, sb, a);
		return sb.toString();
	}
	
	private class Node<E> {
		
		private Node<E>[] children;
		private boolean terminal;
		private E value;
		
		@SuppressWarnings("unchecked")
		public Node(int maxval) {
			children = new Node[maxval];
			terminal = false;
		}
		
		public String toString() {
			return Arrays.toString(children);
		}
	}

}
