package dataStructures;

import java.util.Comparator;

/**
 * Class that represents an ordered pair of integers.
 * 
 * @author f.aubry@uclouvain.be
 */
public class IntPair {
	
	public int x, y;
	
	public IntPair(int x, int y) {
		this.x = x;
		this.y = y;
	}
	
	public boolean equals(Object other) {
		if(other instanceof IntPair) {
			IntPair o = (IntPair)other;
			return x == o.x && y == o.y;
		}
		return false;
	}
	
	public int hashCode() {
		return 23 * x + 31 * y;
	}
	
	public String toString() {
		return String.format("(%d, %d)", x, y);
	}
	
	public static class Xcmp implements Comparator<IntPair> {

		public int compare(IntPair a, IntPair b) {
			return a.x - b.x;
		}

	}

	public static class XcmpRev implements Comparator<IntPair> {

		public int compare(IntPair a, IntPair b) {
			return -(a.x - b.x);
		}

	}
	
	
	public static class Ycmp implements Comparator<IntPair> {

		public int compare(IntPair a, IntPair b) {
			return a.y - b.y;
		}

	}

	public static class YcmpRev implements Comparator<IntPair> {

		public int compare(IntPair a, IntPair b) {
			return -(a.y - b.y);
		}

	}

}
