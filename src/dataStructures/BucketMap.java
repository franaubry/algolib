package dataStructures;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Set;

/**
 * Data structure to aggregate objects by key. Maintains a map K -> list of L so
 * that each key can correspond to several values.
 * 
 * @author f.aubry@uclouvain.be
 */
public class BucketMap<K, V> {

	private HashMap<K, ArrayList<V>> M;

	/*
	 * Create an empty bucket map.
	 */
	public BucketMap() {
		M = new HashMap<>();
	}
	
	/*
	 * Add value v to the list corresponding to key k.
	 */
	public void put(K k, V v) {
		ArrayList<V> L = M.get(k);
		if(L == null) {
			L = new ArrayList<>();
			M.put(k, L);
		}
		L.add(v);
	}
	
	/*
	 * Get the number of values associated with key k.
	 */
	public int size(K k) {
		ArrayList<V> L = M.get(k);
		if(L == null) return 0;
		return L.size();
	}
	
	public void createEmptyBucket(K k) {
		M.put(k, new ArrayList<>());
	}
	
	/*
	 * Check whether the map contains key k.
	 */
	public boolean contains(K k) {
		return M.containsKey(k);
	}
	
	/*
	 * Get the list of values associated with key k.
	 */
	public ArrayList<V> get(K k) {
		return M.get(k);
	}
	
	/*
	 * Get the set of all keys.
	 */
	public Set<K> keySet() {
		return M.keySet();
	}
	
	/*
	 * Create a string representation of the bucket map.
	 */
	public String toString() {
		StringBuilder sb = new StringBuilder();
		for(K k : keySet()) {
			sb.append(k + ": " + M.get(k) + "\n");
		}
		return sb.toString();
	}
	
}
