package graph;

import java.util.Arrays;

public class MinCostFlow {

	private Graph<FlowEdge> g;
	private int maxflow, s, t;
	private double flowcost;
	
	public MinCostFlow(Graph<FlowEdge> g, int s, int t) {
		this.g = g;
		this.s = s;
		this.t = t;
		maxflow = 0;
		flowcost = 0;
		while(augment()) {}
	}
	
	public int maxflow() {
		return maxflow;
	}
	
	public double flowcost() {
		return flowcost;
	}
	
	public boolean augment() {
		// initialize the distance labels
		double[] dist = new double[g.V()];
		Arrays.fill(dist, Double.POSITIVE_INFINITY);
		dist[s] = 0;
		// initialize the parents
		FlowEdge[] parent = new FlowEdge[g.V()];
		// initialize times in count
		// timesIn[v] = number of times v was put into Q
		int[] timesIn = new int[g.V()];
		// initialize the capacity of the shortest paths
		int[] pathcap = new int[g.V()];
		Arrays.fill(pathcap, Integer.MAX_VALUE);
		// initialize the queue
		SetQueue Q = new SetQueue(s);
		boolean finished = false;
		// loop to compute the shortest paths
		while(!finished && !Q.isEmpty()) {
			int u = Q.poll();
			for(FlowEdge e : g.outEdges(u)) {
				if(e.cap() == 0) continue;
				int v = e.dest;
				// check if we found a better path
				if(dist[v] > dist[u] + e.cost()) {
					// update the distance
					dist[v] = dist[u] + e.cost();
					// update the parent
					parent[v] = e;
					// update the path capacity
					pathcap[v] = Math.min(pathcap[u], e.cap());
					if(!Q.contains(v)) {
						// add v to the queue since it is not in yet
						Q.add(v);
						timesIn[v]++;
						if(timesIn[v] == g.V()) {
							// if a node has been V times in we can stop
							finished = true;
							break;
						}
					}
				}
			}
		}
		if(parent[t] == null) return false;
		// push the flow
		int flow = pathcap[t];
		maxflow += flow;
		int cur = t;
		while(parent[cur] != null) {
			flowcost += parent[cur].push(flow);
			cur = parent[cur].orig();
		}
		return true;
	}
	
	
}
