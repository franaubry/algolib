package graph;

import java.util.Arrays;

public class ShortestPathFaster {

	private Graph<? extends WeightedEdge> g;
	private double[] dist;
	private Edge[] parent;
	private int s;
	
	public ShortestPathFaster(Graph<? extends WeightedEdge> g) {
		this.g = g;
	}
	
	public void computeShortestPaths(int s) {
		dist = new double[g.V()];
		Arrays.fill(dist, Double.POSITIVE_INFINITY);
		parent = new Edge[g.V()];
		int[] timesIn = new int[g.V()];
		SetQueue Q = new SetQueue(s);
		while(!Q.isEmpty()) {
			int u = Q.poll();
			for(WeightedEdge e : g.outEdges(u)) {
				if(!e.isActive()) continue;
				int v = e.dest;
				if(dist[v] > dist[u] + e.weight()) {
					dist[v] = dist[u] + e.weight();
					parent[v] = e;
					if(!Q.contains(v)) {
						Q.add(v);
						timesIn[v]++;
						if(timesIn[v] == g.V()) return;
					}
				}
			}
		}
	}
		
}