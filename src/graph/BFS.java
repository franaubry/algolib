package graph;

import java.util.BitSet;
import java.util.LinkedList;
import java.util.Queue;

/**
 * Class used to perform breadth-first search on graphs.
 * 
 * @author f.aubry@uclouvain.be
 */
public class BFS {

	/*
	 * Given a graph, a source node s and a destination node t, compute
	 * whether there exists a path from s to t.
	 */
	public static boolean connected(Graph<? extends Edge> g, int s, int t) {
		Queue<Integer> Q = new LinkedList<>();
		BitSet visited = new BitSet();
		Q.add(s);
		visited.set(s);
		while(!visited.get(t) && !Q.isEmpty()) {
			int cur = Q.poll();
			for(Edge e : g.outEdges(cur)) {
				if(e.isActive() && !visited.get(e.dest())) {
					visited.set(e.dest());
					Q.add(e.dest());
				}
			}
		}
		return visited.get(t);
	}
	
	public static LinkedList<Integer>[] connectedComponents(Graph<? extends Edge> g) {
		BitSet visited = new BitSet();
		int[] component = new int[g.V()];
		int c = 0;
		for(int v = 0; v < g.V(); v++) {
			if(visited.get(v)) continue;
			Queue<Integer> Q = new LinkedList<>();
			Q.add(v);
			visited.set(v);
			while(!Q.isEmpty()) {
				int cur = Q.poll();
				component[cur] = c;
				for(Edge e : g.outEdges(cur)) {
					if(e.isActive() && !visited.get(e.dest())) {
						visited.set(e.dest());
						Q.add(e.dest());
					}
				}
			}
			c++;
		}
		@SuppressWarnings("unchecked")
		LinkedList<Integer>[] cc = new LinkedList[c];
		for(int i = 0; i < c; i++) {
			cc[i] = new LinkedList<>();
		}
		for(int v = 0; v < g.V(); v++) {
			cc[component[v]].add(v);
		}
		return cc;
	}
	
	/*
	 * Given an undirected graph, compute whether there is a path
	 * between each pair of nodes.
	 */
	public static boolean connected(Graph<? extends Edge> g) {
		Queue<Integer> Q = new LinkedList<>();
		BitSet visited = new BitSet();
		Q.add(0);
		visited.set(0);
		while(!Q.isEmpty()) {
			int cur = Q.poll();
			for(Edge e : g.outEdges(cur)) {
				if(e.isActive() && !visited.get(e.dest())) {
					visited.set(e.dest());
					Q.add(e.dest());
				}
			}
		}
		for(int t = 0; t < g.V(); t++) {
			if(!visited.get(t)) return false;
		}
		return true;
	}
	
	/*
	 * Given a graph, a source node s and a destination t, compute
	 * a path from s to t if one exists. Return null if no such path
	 * exists.
	 */
	public static Path findPath(Graph<? extends Edge> g, int s, int t) {
		Integer[] parent = new Integer[g.V()];
		Queue<Integer> Q = new LinkedList<>();
		Q.add(s);
		while(parent[t] == null && !Q.isEmpty()) {
			int cur = Q.poll();
			for(Edge e : g.outEdges(cur)) {
				if(e.isActive() && parent[e.dest()] == null) {
					parent[e.dest()] = e.orig();
					Q.add(e.dest());
				}
			}
		}
		if(parent[t] == null) {
			return null;
		}
		Path path = new Path();
		Integer cur = t;
		while(cur != null) {
			path.add(cur);
			cur = parent[t];
		}
		path.reverse();
		return path;
	}

}
