package graph;
import java.util.Arrays;

import utils.ArraysExt;

public class FloydWarshall {

	public static double[][] apsp(Graph<? extends WeightedEdge> g) {
		int n = g.V();
		double[][] dist = new double[n][n];
		for(int i = 0; i < n; i++) {
			Arrays.fill(dist[i], Double.POSITIVE_INFINITY);
			dist[i][i] = 0;
		}
		for(int i = 0; i < n; i++) {
			for(WeightedEdge e : g.outEdges(i)) {
				dist[e.orig()][e.dest()] = e.weight();
			}
		}
		for(int k = 0; k < n; k++) {
			for(int i = 0; i < n; i++) {
				for(int j = 0; j < n; j++) {
					dist[i][j] = Math.min(dist[i][j], dist[i][k] + dist[k][j]);
				}
			}
		}
		return dist;
	}
	
	public static double[][] apsp(double[][] w) {
		int n = w.length;
		double[][] dist = new double[n][n];
		for(int i = 0; i < n; i++) {
			for(int j = 0; j < n; j++) {
				dist[i][j] = w[i][j];				
			}
		}
		for(int k = 0; k < n; k++) {
			for(int i = 0; i < n; i++) {
				for(int j = 0; j < n; j++) {
					dist[i][j] = Math.min(dist[i][j], dist[i][k] + dist[k][j]);
				}
			}
		}
		return dist;
	}
	
	public static double[][] apsp(Graph<? extends Edge> g, double[] weight) {
		int n = g.V();
		double[][] dist = new double[n][n];
		for(int i = 0; i < n; i++) {
			Arrays.fill(dist[i], Double.POSITIVE_INFINITY);
			dist[i][i] = 0;
		}
		for(int i = 0; i < n; i++) {
			for(Edge e : g.outEdges(i)) {
				dist[e.orig()][e.dest()] = weight[e.id()];
			}
		}
		for(int k = 0; k < n; k++) {
			for(int i = 0; i < n; i++) {
				for(int j = 0; j < n; j++) {
					dist[i][j] = Math.min(dist[i][j], dist[i][k] + dist[k][j]);
				}
			}
		}
		return dist;
	}
	
	public static int center(Graph<? extends Edge> g, double[] weight) {
		double[][] d = apsp(g, weight);
		double diam = Double.POSITIVE_INFINITY;
		int center = -1;
		for(int v = 0; v < g.V(); v++) {
			int u = ArraysExt.argmax(d[v]);
			if(d[v][u] < diam) {
				diam = d[v][u];
				center = u;
			}
		}
		return center;
	}

}
