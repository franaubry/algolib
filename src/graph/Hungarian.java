package graph;

/**
 * 
 * O(V^3) implementation of the Hungarian algorithm.
 * 
 * This implementation is a Java port from the C++ implementation
 * given in:
 * 
 * https://www.topcoder.com/community/data-science/data-science-tutorials/assignment-problem-and-hungarian-algorithm/
 * 
 * @author f.aubry@uclouvain.be
 *
 */
public class Hungarian {

	private double[][] cost;
	private int n;
	private double[] lx, ly;
	private int maxMatch;
	private boolean[] S, T;
	private int[] slackx, prev, xy, yx;
	private double[] slack;
	
	private int[] assignment;
	private double value;
	private boolean minimize;
	
	/**
	 * Builds an instance of the Hungarian algorithm.
	 * 
	 * @param cost the cost matrix
	 * @param minimize if set to true the algorithm will compute the minimum cost assignment
	 * otherwise it will compute the maximum cost assignment
	 */
	public Hungarian(double[][] cost, boolean minimize) {
		this.minimize = minimize;
		int n = cost.length;
		int m = cost[0].length;
		int k = Math.max(n, m);
		double[][] M = new double[k][k];
		for(int i = 0; i < k; i++) {
			for(int j = 0; j < k; j++) {
				if(i < n && j < m) {
					M[i][j] = minimize ? -cost[i][j] : cost[i][j];
				} else {
					M[i][j] = 0.0;
				}
			}
		}
		int[] temp = maxHungarian(M);
		assignment = new int[n];
		for(int i = 0; i < n; i++) {
			assignment[i] = temp[i] < m ? temp[i] : -1;
		}
	}
	
	/**
	 * Get the assignment solution. 
	 * 
	 * The solution is an array assignment with one element
	 * for each node on the left side (one element per row)
	 * such that:
	 * 
	 * assignment[i] = node on the right to which i is assigned
	 * 
	 * If the left side has more nodes than the right side 
	 * (i.e the input matrix has more rows than columns) then 
	 * some nodes will be left unassigned. For such nodes i, we
	 * will have assignment[i] = -1
	 * 
	 * @return assignment array
	 */
	public int[] getAssignment() {
		return assignment;
	}
	
	public int getAssignment(int i) {
		return assignment[i];
	}
	
	/**
	 * Get the value of the optimal assignment.
	 * @return the optimal assignment value
	 */
	public double getAssignmentValue() {
		return minimize ? -value : value;
	}

	private int[] maxHungarian(double[][] M) {
		cost = M;
		n = cost.length;
		slack = new double[n];
		slackx = new int[n];
		prev = new int[n];
		xy = new int[n];
		yx = new int[n];
		maxMatch = 0;
		for(int i = 0; i < n; i++) {
			xy[i] = -1;
			yx[i] = -1;
		}
		initLabels();
		augment();
		value = 0;
		int[] assignment = new int[n];
		for(int x = 0; x < n; x++) {
			value += cost[x][xy[x]];
			assignment[x] = xy[x];
		}
		return assignment;
	}

	private void initLabels() {
		lx = new double[n];
		ly = new double[n];
		for(int x = 0; x < n; x++)
			for(int y = 0; y < n; y++)
				lx[x] = Math.max(lx[x], cost[x][y]);
	}

	private void augment() {
		if(maxMatch == n) {return;}
		int x, y, root = 0;
		int[] q = new int[n];
		int wr = 0, rd = 0;
		S = new boolean[n];
		T = new boolean[n];
		for(x = 0; x < n; x++)
			prev[x] = -1;
		for(x = 0; x < n; x++) {
			if(xy[x] == -1) {
				q[wr++] = root = x;
				prev[x] = -2;
				S[x] = true;
				break;
			}
		}
		for(y = 0; y < n; y++) {
			slack[y] = lx[root] + ly[y] - cost[root][y];
			slackx[y] = root;
		}
		while(true) {                        
			while(rd < wr) {
				x = q[rd++];                        
				for(y = 0; y < n; y++) {                 
					if(cost[x][y] == lx[x]+ly[y] && !T[y]) {
						if(yx[y] == -1) {break;}              
						T[y] = true;                    
						q[wr++] = yx[y];                 
						addToTree(yx[y], x);               
					}
				}
				if (y < n) {break;}
			}
			if (y < n) {break;}
			updateLabels();                         
			wr = rd = 0;        
			for (y = 0; y < n; y++) {    
				if (!T[y] &&  slack[y] == 0) {
					if(yx[y] == -1) {
						x = slackx[y];
						break;
					} else {
						T[y] = true;                     
						if(!S[yx[y]]) {
							q[wr++] = yx[y];                  
							addToTree(yx[y], slackx[y]);                             
						}
					}
				}
			}
			if(y < n) {break;}
		}
		if(y < n) {
			maxMatch++;                        
			for(int cx=x, cy=y, ty; cx!=-2; cx=prev[cx], cy=ty){
				ty = xy[cx];
				yx[cy] = cx;
				xy[cx] = cy;
			}
			augment();                            
		}
	}

	private void updateLabels() {
		double delta = Double.POSITIVE_INFINITY;
		for(int y = 0; y < n; y++)
			if(!T[y])
				delta = Math.min(delta, slack[y]);
		for(int i = 0; i < n; i++) {
			if(S[i]) {lx[i] -= delta;}
			if(T[i]) {ly[i] += delta;}
			if(!T[i]) {slack[i] -= delta;}
		}
	}

	private void addToTree(int x, int prevx) {
		S[x] = true;
		prev[x] = prevx;
		for(int y = 0; y < n; y++) {
			if(lx[x] + ly[y] - cost[x][y] < slack[y]) {
				slack[y] = lx[x] + ly[y] - cost[x][y];
				slackx[y] = x;
			}
		}
	}

}
