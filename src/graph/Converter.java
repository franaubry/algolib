package graph;

public class Converter {

	public static Graph<WeightedEdge> convert(Graph<NetworkEdge> g) {
		Graph<WeightedEdge> gw = new Graph<>(g.V());
		for(NetworkEdge e : g.edges()) {
			WeightedEdge ew = new WeightedEdge(e.orig, e.dest, e.weight(), e.id);
			gw.addEdge(ew, true);
		}
		gw.setName(g.name());
		gw.setNodeLabels(g.getNodeLabels());
		return gw;
	}
	
}
