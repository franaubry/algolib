package graph;

import java.util.Arrays;
import java.util.BitSet;
import java.util.LinkedList;
import java.util.Queue;

/**
 * Implementation of EdmondsKarp algorithm to compute the maximum
 * flow in a graph.
 * 
 * @author f.aubry@uclouvain.be
 */
public class EdmondsKarp<E extends WeightedEdge> {

	public int maxflow;
	public int s, t;
	public Graph<FlowEdge> gr;
	
	/*
	 * Build an EdmondsKarp instance for graph g, source
	 * s and sink t.
	 */
	public EdmondsKarp(Graph<E> g, int s, int t) {
		this.s = s;
		this.t = t;
		gr = new Graph<>(g.V());
		for(E e : g.edges()) {
			FlowEdge fe = new FlowEdge(e.orig(), e.dest(), e.weight(), 0);
			fe.createResidual();
			gr.addEdge(fe);
			gr.addEdge(fe.getResidual());
		}
		edmondsKarp();
	}
	
	/*
	 * Auxiliary method to compute the maximum s-t flow.
	 */
	private void edmondsKarp() {
		maxflow = 0;
		while(true) {
			// initialize BFS data		
			FlowEdge[] parent = new FlowEdge[gr.V()];
			int[] pcap = new int[gr.V()];
			Arrays.fill(pcap, Integer.MAX_VALUE);
			BitSet vis = new BitSet();
			vis.set(s);
			Queue<Integer> Q = new LinkedList<>();
			Q.add(s);
			// perform BFS
			while(!Q.isEmpty()) {
				int u = Q.poll();
				for(FlowEdge e : gr.outEdges(u)) {
					if(e.cap() > 0 && !vis.get(e.dest())) {
						vis.set(e.dest());
						Q.add(e.dest());
						parent[e.dest()] = e;
						pcap[e.dest()] = Math.min(pcap[e.orig()], e.cap());
					}
				}
			}
			if(!vis.get(t)) break;
			// augment the path
			FlowEdge e = parent[t];
			int flow = pcap[t];
			while(e != null) {
				e.push(flow);
				e = parent[e.orig()];
			}
			maxflow += flow;
		}	
	}
	
}

