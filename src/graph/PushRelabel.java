package graph;

public class PushRelabel {
	
	private int n;
	private int[][] cap;
	private int[] h, e;
	
	public PushRelabel(int[][] cap, int s, int t) {
		n = cap.length;
		this.cap = cap;
		h = new int[n];
		h[s] = n;
		e = new int[n];
		for(int v = 0; v < n; v++) {
			if(cap[s][v] > 0) {
				e[v] += cap[s][v];
				e[s] -= cap[s][v];
			}
		}
		while(true) {
			for(int u = 0; u < n; u++) {
				for(int v = 0; v < n; v++) {
					
				}
			}
		}
	}
	
	public void push(int u, int v) {
		int flow = Math.min(e[u], cap[u][v]);
		e[u] -= flow;
		e[v] += flow;
	}
	
	public void relabel(int u) {
		int minh = Integer.MAX_VALUE;
		for(int v = 0; v < n; v++) {
			if(cap[u][v] > 0) {
				minh = Math.min(minh, h[v]);
			}
		}
		h[u] = 1 + minh;
	}
	
	
	
}
