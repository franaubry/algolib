package graph;

import java.util.Arrays;

/**
 * Class used to compute longest paths in DAGs.
 * 
 * @author f.aubry@uclouvain.be
 */
public class DagLongestPath {

	/*
	 * Given a weighted directed acyclic graph and a source s, computes
	 * the longest path from s to each other node. Outputs an array d
	 * such that d[x] is the length of the longest path between s and x.
	 */
	public static int[] longestPathDistances(Graph<? extends WeightedEdge> g, int s) {
		TopologicalSort<? extends WeightedEdge> ts = new TopologicalSort<>(g);
		int[] distance = new int[g.V()]; 
		Arrays.fill(distance, Integer.MIN_VALUE);
		distance[s] = 0;
		int is = ts.position[s];
		for(int i = is; i < g.V(); i++) {
			int x = ts.order[i];
			for(WeightedEdge e : g.outEdges(x)) {
				int y = e.dest();
				distance[y] = Math.max(distance[y], e.weight() + distance[x]);
			}
		}
		return distance;
	}
	
	/*
	 * Given a graph, a source s and edge weights, computes the
	 * longest path from s to each other node. Outputs an array d
	 * such that d[x] is the length of the longest path between s and x.
	 */
	public static double[] longestPathDistances(Graph<? extends Edge> g, int s, double[] weight) {
		TopologicalSort<? extends Edge> ts = new TopologicalSort<>(g);
		double[] distance = new double[g.V()]; 
		Arrays.fill(distance, Double.NEGATIVE_INFINITY);
		distance[s] = 0;
		int is = ts.position[s];
		for(int i = is; i < g.V(); i++) {
			int x = ts.order[i];
			for(Edge e : g.outEdges(x)) {
				int y = e.dest();
				distance[y] = Math.max(distance[y], weight[e.id] + distance[x]);
			}
		}
		return distance;
	}
	
	
}
