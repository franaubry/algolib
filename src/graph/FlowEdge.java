package graph;

/**
 * Class that represents and edge in a flow network.
 * 
 * @author f.aubry@uclouvain.be
 */
public class FlowEdge extends Edge {
	
	private int flow;
	private int cap;
	private double cost;
	private FlowEdge residual;
	private boolean isResidual;
	
	public FlowEdge(int orig, int dest, int cap, double cost) {
		super(orig, dest);
		this.cap = cap;
		this.cost = cost;
		flow = 0;
		isResidual = false;
	}
	
	public FlowEdge createResidual() {
		residual = new FlowEdge(dest, orig, 0, -cost);
		residual.isResidual = true;
		residual.residual = this;
		return residual;
	}
	
	public boolean isResidual() {
		return isResidual;
	}
	
	public FlowEdge getResidual() {
		return residual;
	}
	
	public int flow() {
		return flow;
	}
	
	public int cap() {
		return cap;
	}
	
	public double cost() {
		return cost;
	}
	
	public double push(int flow) {
		this.flow += flow;
		cap -= flow;
		residual.cap += flow;
		return flow * cost;
	}

}