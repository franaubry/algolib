package graph;

public class FlowGraphBuilder {
	
	protected Graph<FlowEdge> g;
	protected int s, t;
	
	public FlowGraphBuilder() {}
	
	public FlowGraphBuilder(int V) {
		g = new Graph<>(V);
	}
	
	public FlowGraphBuilder(int V, int s, int t) {
		g = new Graph<>(V);
		this.s = s;
		this.t = t;
	}
	
	public void initGraph(int V) {
		g = new Graph<>(V);
	}
	
	public void setSource(int s) {
		this.s = s;
	}
	
	public void setSink(int t) {
		this.t = t;
	}
	
	public int getSource() {
		return s;
	}
	
	public int getSink() {
		return t;
	}
	
	public void addEdge(int orig, int dest, int cap, double cost) {
		FlowEdge fe = new FlowEdge(orig, dest, cap, cost);
		fe.createResidual();
		g.addEdge(fe);
		g.addEdge(fe.getResidual());
	}
	
	public void addEdgeUnd(int u, int v, int cap, double cost) {
		addEdge(u, v, cap, cost);
		addEdge(v, u, cap, cost);
	}
	
	public Graph<FlowEdge> getGraph() {
		return g;
	}

}
