package graph;

import java.util.BitSet;
import java.util.LinkedList;
import java.util.Queue;

public class SetQueue {
	
	private Queue<Integer> Q;
	private BitSet inQ;
	
	public SetQueue() {
		Q = new LinkedList<>();
		inQ = new BitSet();
	}
	
	public SetQueue(int s) {
		Q = new LinkedList<>();
		inQ = new BitSet();
		add(s);
	}
	
	public boolean isEmpty() {
		return Q.isEmpty();
	}
	
	public int poll() {
		int ret = Q.poll();
		inQ.clear(ret);
		return ret;
	}
	
	public void add(int v) {
		if(!inQ.get(v)) {
			inQ.set(v);
			Q.add(v);
		}
	}
	
	public boolean contains(int v) {
		return inQ.get(v);
	}
	
	public String toString() {
		return Q.toString();
	}

}
