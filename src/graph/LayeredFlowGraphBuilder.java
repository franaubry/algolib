package graph;

import dataStructures.IntPair;

/**
 * 
 * Build a layered flow graph given the layer sizes.
 * 
 * firstIndex[0] = 0
 * firstIndex[i] = firstIndex[i - 1] + layerSize[i - 1]
 * 
 * S and T are the two last index nodes.
 * 
 * 
 * 
 *    L1_1                L2_1
 *    L1_2                L2_2
 * S  ...                 ...                   ...     T
 *    L1_layerSizers[0]   L2_layerSizers[1]
 * 
 * 
 * @author f.aubry@uclouvain.be
 *
 */
public class LayeredFlowGraphBuilder extends FlowGraphBuilder {

	private int[] firstIndex;
	
    public LayeredFlowGraphBuilder(int[] layerSizes) {
		super();
		firstIndex = new int[layerSizes.length];
		int V = 0;
		for(int i = 0; i < layerSizes.length; i++) {
			firstIndex[i] = V;
			V += layerSizes[i];
		}
		V += 2;
		initGraph(V);
		setSource(V - 2);
		setSink(V - 1);
	}
    
    public IntPair getLayerAndIndex(int v) {
    	int layer = getLayer(v);
    	return new IntPair(layer, v - firstIndex[layer]);
    }
    
    public int getIndexInLayer(int v) {
    	int layer = getLayer(v);
    	return v - firstIndex[layer];
    }
    
    public int getLayer(int v) {
    	int lb = 0;
    	int ub = firstIndex.length - 1;
    	while(ub - lb > 1) {
    		int mid = (lb + ub) / 2;
    		if(firstIndex[mid] > v) {
    			ub = mid - 1;
    		} else {
    			lb = mid;
    		}
    	}
    	if(firstIndex[ub] > v) return lb;
    	return ub;
    }
    
    public void connectSource(int indexInLayer1, int cap, double cost) {
    	super.addEdge(s, indexInLayer1, cap, cost);
    }
    
    public void connectSink(int indexInLastLayer, int cap, double cost) {
    	super.addEdge(firstIndex[firstIndex.length - 1] + indexInLastLayer, t, cap, cost);
    }
    
    public void connectSourceUnd(int indexInLayer1, int cap, double cost) {
    	super.addEdgeUnd(s, indexInLayer1, cap, cost);
    }
    
    public void connectSinkUnd(int indexInLastLayer, int cap, double cost) {
    	super.addEdgeUnd(firstIndex[firstIndex.length - 1] + indexInLastLayer, t, cap, cost);
    }
    
    public void addEdge(int layer1, int indexInLayer1, int layer2, int indexInLayer2, int cap, double cost) {
    	super.addEdge(firstIndex[layer1] + indexInLayer1, firstIndex[layer2] + indexInLayer2, cap, cost);
    }
    
    public void addEdgeUnd(int layer1, int indexInLayer1, int layer2, int indexInLayer2, int cap, double cost) {
    	super.addEdgeUnd(firstIndex[layer1] + indexInLayer1, firstIndex[layer2] + indexInLayer2, cap, cost);
    }


}
