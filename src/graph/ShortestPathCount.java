package graph;

public class ShortestPathCount {
	
	/*
	 * Count the number of shortest paths that go through each node.
	 * pathcount[v] = number of shortest paths through v
	 */
	public static int[] shortestPathCount(Graph<? extends WeightedEdge> g) {
		int[] pathcount = new int[g.V()];
		for(int v = 0; v < g.V(); v++) {
			Graph<WeightedEdge> dagV = Dijkstra.shortestPathFrom(g, v).dag;
			for(int u = 0; u < g.V(); u++) {
				pathcount[u] += dagV.outDeg(u) + dagV.inDeg(u);
			}
		}
		return pathcount;
	}
	
	/*
	 * Count the number of non-empty shortest paths that go through each node.
	 * pathcount[v] = number of non-empty shortest paths through v
	 */
	public static int[] nonEmptyShortestPathCount(Graph<? extends WeightedEdge> g) {
		int[] pathcount = new int[g.V()];
		for(int v = 0; v < g.V(); v++) {
			Graph<WeightedEdge> dagV = Dijkstra.shortestPathFrom(g, v).dag;
			for(int u = 0; u < g.V(); u++) {
				pathcount[u] += dagV.outDeg(u) + dagV.inDeg(u);
			}
			pathcount[v]--;
		}
		return pathcount;
	}

	
	/*
	 * Compute an array, demandThrough such that demandThrough[v] is the
	 * number of pairs (x, y) with x != y such that at least one shortest path from
	 * x to y passes by v
	 */
	public static int[] demandsThrough(double[][] d) {
		int[] demandsThrough = new int[d.length];
		for(int x = 0; x < d.length; x++) {
			for(int y = 0; y < d.length; y++) {
				if(x == y) continue;
				for(int v = 0; v < d.length; v++) {
					if(Math.abs(d[x][v] + d[v][y] - d[x][y]) < 1e-9) {
						demandsThrough[v] += 1;
					}
				}
			}
		}
		return demandsThrough;
	}

}
