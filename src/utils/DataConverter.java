package utils;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.LinkedList;

public class DataConverter {

	public static LinkedList<Integer> hashSetToLinkedList(HashSet<Integer> H) {
		LinkedList<Integer> L = new LinkedList<>();
		L.addAll(H);
		return L;
	}
	
	public static ArrayList<Integer> hashSetToArrayListList(HashSet<Integer> H) {
		ArrayList<Integer> L = new ArrayList<>();
		L.addAll(H);
		return L;
	}
	
}
