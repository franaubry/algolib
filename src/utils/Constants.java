package utils;

public class Constants {

	public static int[][] dir = new int[][] {
		new int[] {-1, 0},
		new int[] {1, 0},
		new int[] {0, -1},
		new int[] {0, 1}
	};
	
	public static double eps = 1e-8;
	
}
