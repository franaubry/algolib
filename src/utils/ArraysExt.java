package utils;

import java.util.ArrayList;

public class ArraysExt {
	
	/*
	 * Compute the maximum value in the input array.
	 * If an empty array is given it returns Integer.MIN_VALUE.
	 */
	public static int max(int[] a) {
		int max = Integer.MIN_VALUE;
		for(int i = 0; i < a.length; i++) {
			max = Math.max(max, a[i]);
		}
		return max;
	}
	
	/*
	 * Compute the sum of the values of the input array.
	 */
	public static int sum(int[] a) {
		int sum = 0;
		for(int i = 0; i < a.length; i++) {
			sum += a[i];
		}
		return sum;
	}
	
	/*
	 * Compute the first i such that a[i] = min_j a[j]
	 */
	public static int argmin(double[] a) {
		double min = Double.POSITIVE_INFINITY;
		int imin = -1;
		for(int i = 0; i < a.length; i++) {
			if(a[i] < min) {
				min = a[i];
				imin = i;
			}
		}
		return imin;
	}
	
	/*
	 * Compute the first i such that a[i] = max_j a[j]
	 */
	public static int argmax(double[] a) {
		double max = Double.NEGATIVE_INFINITY;
		int imax = -1;
		for(int i = 0; i < a.length; i++) {
			if(a[i] > max) {
				max = a[i];
				imax = i;
			}
		}
		return imax;
	}
	
	/*
	 * Compute the first i such that a[i] = min_{j in sub} a[j]
	 */
	public static int argmin(double[] a, ArrayList<Integer> sub) {
		double min = Double.POSITIVE_INFINITY;
		int imin = -1;
		for(int i : sub) {
			if(a[i] < min) {
				min = a[i];
				imin = i;
			}
		}
		return imin;
	}
	
	/*
	 * Compute the sum of all elements in the given matrix
	 */
	public static double sum(double[][] a) {
		double sum = 0;
		for(int i = 0; i < a.length; i++) {
			for(int j = 0; j < a[i].length; j++) {
				sum += a[i][j];
			}
		}
		return sum;
	}

	
	/*
	 * Compute the maximum of all elements in the given matrix
	 */
	public static double max(double[][] a) {
		double max = Double.NEGATIVE_INFINITY;
		for(int i = 0; i < a.length; i++) {
			for(int j = 0; j < a[i].length; j++) {
				max = Math.max(max, a[i][j]);
			}
		}
		return max;
	}

	/*
	 * Compute the maximum of all elements in the given matrix
	 */
	public static int max(int[][] a) {
		int max = Integer.MIN_VALUE;
		for(int i = 0; i < a.length; i++) {
			for(int j = 0; j < a[i].length; j++) {
				max = Math.max(max, a[i][j]);
			}
		}
		return max;
	}
	
	public static boolean equalMatrix(double[][] A, double[][] B) {
		if(A.length != B.length) return false;
		int n = A.length;
		for(int i = 0; i < n; i++) {
			if(A[i].length != B[i].length) return false;
		}
		int m = A[0].length;
		for(int i = 0; i < n; i++) {
			for(int j = 0; j < m; j++) {
				if(!Cmp.eq(A[i][j], B[i][j])) {
					return false;
				}
			}
		}
		return true;
	}
	
	public static int[] convert(Integer[] a, int nullReplacement) {
		int[] b = new int[a.length];
		for(int i = 0; i < a.length; i++) {
			b[i] = a[i] == null ? nullReplacement : a[i];
		}
		return b;
	}
	
	public static int firstIndexOf(int[] a, int target) {
		for(int i = 0; i < a.length; i++) {
			if(a[i] == target) {
				return i;
			}
		}
		return -1;
	}
	
	public static boolean contains(int[] a, int target) {
		return firstIndexOf(a, target) != -1;
	}
	
	public static int[][] copyMatrix(int[][] m) {
		int[][] mc = new int[m.length][m[0].length];
		for(int i = 0; i < m.length; i++) {
			for(int j = 0; j < m[i].length; j++) {
				mc[i][j] = m[i][j];
			}
		}
		return mc;
	}
	
	public static Integer[] copyArray(Integer[] a) {
		Integer[] ac = new Integer[a.length];
		for(int i = 0; i < a.length; i++) {
			ac[i] = a[i];
		}
		return ac;
	}
	
	public static int[] copyArray(int[] a) {
		int[] ac = new int[a.length];
		for(int i = 0; i < a.length; i++) {
			ac[i] = a[i];
		}
		return ac;
	}
	
	public static String matrixToString(int[][] m) {
		StringBuilder sb = new StringBuilder();
		for(int i = 0; i < m.length; i++) {
			for(int j = 0; j < m[i].length; j++) {
				sb.append(m[i][j]);
				if(j < m[i].length - 1) sb.append(' ');
			}
			sb.append('\n');
		}
		return sb.toString();
	}
	
	public static String matrixToString(double[][] m) {
		StringBuilder sb = new StringBuilder();
		for(int i = 0; i < m.length; i++) {
			for(int j = 0; j < m[i].length; j++) {
				sb.append(String.format("%.2f", m[i][j]));
				if(j < m[i].length - 1) sb.append(' ');
			}
			sb.append('\n');
		}
		return sb.toString();
	}
	
	public static String arrayToString(int[] a, String prefix, String separator, String suffix) {
		StringBuilder sb = new StringBuilder();
		sb.append(prefix);
		for(int i = 0; i < a.length; i++) {
			sb.append(a[i]);
			if(i < a.length - 1) {
				sb.append(separator);
			}
		}
		sb.append(suffix);
		return sb.toString();
	}
	
	public static String arrayToString(Integer[] a, String prefix, String separator, String suffix) {
		StringBuilder sb = new StringBuilder();
		sb.append(prefix);
		for(int i = 0; i < a.length; i++) {
			sb.append(a[i]);
			if(i < a.length - 1) {
				sb.append(separator);
			}
		}
		sb.append(suffix);
		return sb.toString();
	}
	
	/**
	 * Get an array of a given size with all entries equal to a given value.
	 * 
	 * @param size the size of the array.
	 * @param value the value to set.
	 * @return an array of the given size with all entries equal to value.
	 */
	public static int[] createConstantArray(int size, int value) {
		int[] a = new int[size];
		for(int i = 0; i < size; i++) {
			a[i] = value;
		}
		return a;
	}

	/**
	 * Given two arrays of integers check whether they are equal.
	 * 
	 * @param a the first array.
	 * @param b the second array.
	 * @return whether a and b have the same size and the same elements in order.
	 */
	public static boolean equalArray(int[] a, int[] b) {
		if(a.length != b.length) return false;
		for(int i = 0; i < a.length; i++) {
			if(a[i] != b[i]) return false;
		}
		return true;
	}
	
	/**
	 * Finds the largest index of an element that is greater than or equal to
	 * a given key.
	 * 
	 * @param a an non-increasing array a
	 * @param k the key we seek
	 * @return the largest i such that a[i] >= k or -1 if no such i exists
	 */
	public static int rightmostGeq(int[] a, int k) {
		int lb = 0;
		int ub = a.length - 1;
		while(ub - lb > 1) {
			int mid = (lb + ub) / 2;
			if(a[mid] >= k) {
				lb = mid;
			} else {
				ub = mid - 1;
			}
		}
		if(a[ub] >= k) return ub;
		if(a[lb] >= k) return lb;
		return -1;
	}
	
}
