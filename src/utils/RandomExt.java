package utils;
import java.util.HashSet;
import java.util.Random;

import dataStructures.IntPair;

public class RandomExt {

	private Random rnd;

	public RandomExt(int seed) {
		rnd = new Random(seed);
	}

	public RandomExt() {
		rnd = new Random();
	}

	/*
	 * Generate a random integer in [0, ub].
	 */
	public int randint(int ub) {
		return rnd.nextInt(ub + 1);
	}

	/*
	 * Generate a random index of a.
	 */
	public int randindex(int[] a) {
		return rnd.nextInt(a.length);
	}
	/*
	 * Generate a random integer in [lb, ub].
	 * @PRE: lb <= ub
	 */
	public int randint(int lb, int ub) {
		if(lb > ub) throw new IllegalArgumentException(String.format("lb and ub do not satisfy lb <= ub: lb=%d and ub=%d", lb, ub));
		return rnd.nextInt(ub - lb + 1) + lb;
	}

	/*
	 * Generate a random integer
	 */
	public int randint() {
		return rnd.nextInt();
	}

	/*
	 * Generate a random pair (x, y) with x != y
	 */
	public IntPair genPair(int lb, int ub) {
		int x = randint(lb, ub);
		int y = randint(lb, ub);
		while(x == y) {
			y = randint(lb, ub);
		}
		return new IntPair(x, y);
	}

	/*
	 * Generate a random set of given size.
	 */
	public HashSet<Integer> randset(int lb, int ub, int size) {
		// compute the sample size
		int N = ub - lb + 1;
		// compute the probability of generating the last element
		// at this point we already generated size - 1 elements
		// so there are N - size + 1 changes out of N
		double lastprob = (N - size + 1) / (double)N;
		// compute the number of iterations we need to have
		// probability >= 0.99 to finish the set
		double k = Math.log(0.01) / Math.log(1 - lastprob);
		HashSet<Integer> S = new HashSet<>();
		if(k > 2 * N) {
			// we need more iterations than twice the size of the sample size
			// generate by shuffle
			int[] sample = new int[N];
			for(int i = 0; i < N; i++) {
				sample[i] = lb + i;
			}
			shuffle(sample);
			for(int i = 0; i < size; i++) {
				S.add(sample[i]);
			}
		} else {
			while(S.size() < size) {
				S.add(randint(lb, ub));
			}
		}
		return S;
	}

	/*
	 *  Fisher–Yates shuffle.
	 */
	public void shuffle(int[] a) {
		for (int i = a.length - 1; i > 0; i--) {
			int index = rnd.nextInt(i + 1);
			// Simple swap
			int tmp = a[index];
			a[index] = a[i];
			a[i] = tmp;
		}
	}

}
