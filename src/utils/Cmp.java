package utils;

public class Cmp {

	public static double eps = 1e-8;
	
	public static boolean eq(double x, double y) {
		return Math.abs(x - y) <= eps;
	}
	
	public static boolean geq(double x, double y) {
		return x >= y - eps;
	}
	
	public static boolean gr(double x, double y) {
		return x > y + eps;
	}
	
}
