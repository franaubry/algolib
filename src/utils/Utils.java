package utils;

import java.util.LinkedList;

public class Utils {

	public static LinkedList<Integer> listFirstN(int n) {
		LinkedList<Integer> L = new LinkedList<>();
		for(int i = 0; i < n; i++) {
			L.add(i);
		}
		return L;
	}
	
	public static LinkedList<Integer> singletonList(int v) {
		LinkedList<Integer> L = new LinkedList<>();
		L.add(v);
		return L;
	}
	
}
