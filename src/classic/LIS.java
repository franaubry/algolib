package classic;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;

public class LIS {

	public static void main(String[] args) {
		normalize(new int[] {10, 1, 5, 1, 4});
	}
	
	public static ArrayList<Integer> lis(int[] a, boolean allowEq) {
		ArrayList<Integer> L = new ArrayList<>();
		for(int i = 0; i < a.length; i++) {
			int j = Collections.binarySearch(L, a[i]);
			if(j < 0) j = -(j + 1);
			if(j == L.size()) L.add(a[i]);
			else if(L.get(j) > a[i]) L.set(j, a[i]);
		}
		return L;
	}
	
	public static ArrayList<Integer> lis(ArrayList<Integer> a, boolean allowEq) {
		ArrayList<Integer> L = new ArrayList<>();
		for(int i = 0; i < a.size(); i++) {
			int j = Collections.binarySearch(L, a.get(i));
			if(j < 0) j = -(j + 1);
			if(j == L.size()) L.add(a.get(i));
			else if(L.get(j) > a.get(i) || (allowEq && L.get(j) == a.get(i))) L.set(j, a.get(i));
		}
		return L;
	}

	public static ArrayList<Integer> lds(ArrayList<Integer> a) {
		ArrayList<Integer> L = new ArrayList<>();
		for(int i = 0; i < a.size(); i++) {
			int j = Collections.binarySearch(L, -a.get(i));
			if(j < 0) j = -(j + 1);
			if(j == L.size()) L.add(-a.get(i));
			else if(L.get(j) > -a.get(i) ||) L.set(j, -a.get(i));
		}
		return L;
	}
	
	private static ArrayList<Integer> normalize(int[] a) {
		ArrayList<Pair> pairs = new ArrayList<Pair>();
		for(int i = 0; i < a.length; i++) {
			pairs.add(new Pair(a[i], i));
		}
		Collections.sort(pairs);
		int[] norm = new int[a.length];
		for(int i = 0; i < a.length; i++) {
			norm[pairs.get(i).index] = i;
		}
		System.out.println(Arrays.toString(norm));
		return null;
	}
	
	private static class Pair implements Comparable<Pair> {
		
		public int value, index;
		
		public Pair(int value, int index) {
			this.value = value;
			this.index = index;
		}

		public int compareTo(Pair o) {
			if(value == o.value) return index - o.index;
			return value - o.value;
		}
		
		public String toString() {
			return String.format("(%d, %d)", value, index);
		}
		
	}

 	
}
