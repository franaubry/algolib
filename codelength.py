import os
rootdir = './src/'

linecount = 0
for subdir, dirs, files in os.walk(rootdir):
  for file in files:
    f = open(os.path.join(subdir, file), 'r')
    for line in f.readlines():
      line = line.strip()
      if len(line) > 0:
        linecount += 1
print(linecount)
